from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList
from .forms import TodoListForm

# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    # context = {"todolists": todolists}
    return render(request, 'todos/todo_list.html', {'todo_lists': todo_lists})

def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    return render(request, 'todos/todo_list_detail.html', {'todolist': todolist})

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/todo_list_create.html', {'form': form})
